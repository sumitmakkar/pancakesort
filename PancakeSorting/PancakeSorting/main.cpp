#include <iostream>
#include <vector>

using namespace std;

class Engine
{
    private:
        vector<int> listVector;
    
        int findIndexOfLargestElementInList(int tillIndex)
        {
            int largestNumIndex = 0;
            for(int i = 0 ; i <= tillIndex ; i++)
            {
                largestNumIndex = listVector[largestNumIndex] < listVector[i] ? i : largestNumIndex;
            }
            return largestNumIndex;
        }
    
        void reverseTillIndex(int index)
        {
            int start = 0;
            int end   = index;
            while(start < end)
            {
                int temp          = listVector[start];
                listVector[start] = listVector[end];
                listVector[end]   = temp;
                start++;
                end--;
            }
        }
    
        void display()
        {
            int len = (int)listVector.size();
            for(int i = 0 ; i < len ; i++)
            {
                cout<<listVector[i]<<" ";
            }
            cout<<endl;
        }
    
    public:
        Engine(vector<int> lV)
        {
            listVector = lV;
        }
    
        void pancakeSort()
        {
            int loopSize = (int)listVector.size() - 1;
            while(loopSize)
            {
                reverseTillIndex(findIndexOfLargestElementInList(loopSize));
                reverseTillIndex(loopSize);
                loopSize--;
            }
            display();
        }
};

int main(int argc, const char * argv[])
{
//    vector<int> listVector = {4 , 1 , 2 , 5 , 3};
    vector<int> listVector = {19 , 23 , 6 , 15 , 45 , 30 , 14};
    Engine      e          = Engine(listVector);
    e.pancakeSort();
    return 0;
}
